# hlog - High-performance logger for Go

## Logger 的需求

- 统一要求
    1. 基于 go.uber.org/zap 框架
    2. 文件记录基于 gopkg.in/natefinch/lumberjack.v2 包
    3. 支持 debug 开关，即对于 DPanic 有效
    4. 支持 console 和文件作为输出目标
    5. 可甜可咸
- 关于 level
    1. 支持 level 的统一控制，即低于 level 均不输出到任何目标
    2. logger 的 level 在初始化后，可以动态调整
    3. logger 可以**派生**，派生的 logger 和 root 对输出目标并发写无冲突，level 可以**自由**、**独立**定义
- Console 日志
    1. 日志**一定**会输出到 console
    2. 三种格式可选：彩色 tab 分隔、单色 tab 分隔、JSON
    3. 满足 level 的前提下，Error 或以上级别输出到 stderr，以下输出到 stdout
- 文件日志
    1. 日志**可选配**输出到指定路径
    2. 两种格式可选：单色 tab 分隔、JSON
    3. 文件 rotation 策略可指定：超过多少切割，是否压缩，是否删除最旧的

e.g.

console 格式

```bash
2022-04-14T23:03:25.858+0800	WARN	onelog/main.go:53	Hello Go W World from 2 ~~~~~	{"num": 2}
2022-04-14T23:03:25.961+0800	DEBUG	onelog/main.go:58	Hello Go D World from 2 Now1	{"num": 2}
```

json 格式

```json
{"level":"warn","time":"2022-04-14T23:04:18.485+0800","caller":"onelog/main.go:53","msg":"Hello Go W World from 2 ~~~~~","num":2}
{"level":"debug","time":"2022-04-14T23:04:18.585+0800","caller":"onelog/main.go:58","msg":"Hello Go D World from 2 Now1","num":2}
```
