package hlog

import (
	"io"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Option is a function that configures the logger.
type Option func(*config)

func WithLevel(level zapcore.Level) Option {
	return func(c *config) { c.defaultLevel = level }
}

// WithDevelopmentMode configures the development mode.
func WithDevelopmentMode() Option {
	return func(c *config) { c.development = true }
}

// WithNormalLogOutput configures the io.Writer for logs under error level.
func WithNormalLogOutput(w io.Writer) Option {
	return func(c *config) {
		if w != nil {
			c.normalLogOutput = zapcore.AddSync(w)
		} else {
			c.normalLogOutput = writeDiscard
		}
	}
}

// WithErrorLogOutput configures the io.Writer for logs at or above error level.
func WithErrorLogOutput(w io.Writer) Option {
	return func(c *config) {
		if w != nil {
			c.errorLogOutput = zapcore.AddSync(w)
		} else {
			c.errorLogOutput = writeDiscard
		}
	}
}

// WithConsoleLogFormat configures the format of the console log output.
func WithConsoleLogFormat(format LogFormat) Option {
	return func(c *config) { c.consoleFormat = format }
}

// WithOutputFile configures the output file path.
func WithOutputFile(p string) Option {
	return func(c *config) { c.outputFilePath = p }
}

// WithOutputFileLogFormat configures the format of the output file log output.
func WithOutputFileLogFormat(format LogFormat) Option {
	return func(c *config) { c.outputFileFormat = format }
}

// WithOutputFileMaxSizeInMB configures the max size of the output file in MB.
func WithOutputFileMaxSizeInMB(s uint) Option {
	return func(c *config) { c.outputFileMaxSizeInMB = s }
}

// WithOutputFileCompress configures whether to compress the output file.
func WithOutputFileCompress(b bool) Option {
	return func(c *config) { c.outputFileCompress = b }
}

// WithOutputFileRetainNum configures the number of output files to retain, 0 means unlimited.
func WithOutputFileRetainNum(n uint) Option {
	return func(c *config) { c.outputFileRetainNum = n }
}

// WithStacktraceLevel configures a logger to record a stack trace for all messages at or above a given level.
func WithStacktraceLevel(lvl zapcore.Level) Option {
	return func(c *config) {
		c.stacktraceLevel = &lvl
	}
}

// WithStacktraceLevelString configures a logger to record a stack trace for all messages at or above a given level, if the level string is invalid, it will be ignored.
func WithStacktraceLevelString(text string) Option {
	return func(c *config) {
		lvl, err := zapcore.ParseLevel(text)
		if err == nil {
			c.stacktraceLevel = &lvl
		}
	}
}

// WithMonochrome configures the monochrome mode of console output.
func WithMonochrome(b bool) Option {
	return func(c *config) { c.monochrome = b }
}

// WithZapOptions wraps the zap.Options to configure the logger.
func WithZapOptions(opts ...zap.Option) Option {
	return func(c *config) { c.zapOpts = opts }
}
