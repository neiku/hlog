package hlog

import (
	"bytes"
	"encoding/json"
	"math"
	"reflect"
	"strings"
	"testing"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type logData struct {
	Level  string `json:"level"`
	Time   string `json:"time"`
	Logger string `json:"logger"`
	Caller string `json:"caller"`
	Msg    string `json:"msg"`
}

func getLoggerOutput(l *Logger, b *bytes.Buffer, level zapcore.Level, text string) []byte {
	b.Reset()
	defer b.Reset()
	switch level {
	case zapcore.DebugLevel:
		l.Debugw(text)
	case zapcore.InfoLevel:
		l.Infow(text)
	case zapcore.ErrorLevel:
		l.Errorw(text)
	case zapcore.DPanicLevel:
		l.DPanicf(text)
	case zapcore.PanicLevel:
		l.Panicw(text)
	case zapcore.FatalLevel:
		l.Fatalw(text)
	}
	return b.Bytes()
}

func checkLevel(t *testing.T, loggerName string, l *Logger, b *bytes.Buffer, level zapcore.Level) {
	check := func(lv zapcore.Level) {
		data := getLoggerOutput(l, b, lv, lv.String())
		if level.Enabled(lv) && len(data) == 0 {
			t.Errorf("logger %s level: %s, expect %s log, got empty", loggerName, level, lv)
			t.FailNow()
		}
		if !level.Enabled(lv) && len(data) > 0 {
			t.Errorf("logger %s level: %s, expect empty, got %s log: %v", loggerName, level, lv, b.String())
			t.FailNow()
		}
	}
	// debug
	check(zapcore.DebugLevel)
	check(zapcore.InfoLevel)
	check(zapcore.ErrorLevel)
	// yore: ?
	// l.DPanicw("dPanic")
	// yore: ?
	// l.Panicw("panic")
}

func checkName(t *testing.T, loggerName string, l *Logger, b *bytes.Buffer, name string) {
	data := getLoggerOutput(l, b, zapcore.ErrorLevel, "check name")
	ld := &logData{}
	if err := json.Unmarshal(data, ld); err != nil {
		t.Errorf("%s: unmarshal log failed: %v: %s", loggerName, err, string(data))
		t.FailNow()
	}
	if ld.Logger != name {
		t.Errorf("%s: invalid logger name: got: %v, expected: %v", loggerName, ld.Logger, name)
		t.FailNow()
	}
}

func checkFields(t *testing.T, _ string, l *Logger, b *bytes.Buffer, expected []zap.Field, unexpected []zap.Field) {
	data := getLoggerOutput(l, b, zapcore.ErrorLevel, "check fields")
	logData := make(map[string]interface{}, 0)
	if err := json.Unmarshal(data, &logData); err != nil {
		t.Errorf("unmarshal log data failed: %v", err)
		t.FailNow()
	}

	// check expected fields
	for _, v := range expected {
		d, exist := logData[v.Key]
		if !exist {
			t.Errorf("field is not found: expect: %v", v.Key)
			t.Logf("log is: %s", string(data))
			t.FailNow()
		}
		switch v.Type {
		case zapcore.Int64Type:
			i, ok := d.(float64)
			if !ok {
				t.Errorf("invalid key type: key: %v, got: %T, expected: float64", v.Key, d)
				t.FailNow()
			}
			exist = v.Integer == int64(i)
		case zapcore.BoolType:
			b, ok := d.(bool)
			if !ok {
				t.Errorf("invalid key type: key: %v, got: %T, expected: bool", v.Key, d)
				t.FailNow()
			}
			exist = b == (v.Integer == 1)
		case zapcore.StringType:
			s, ok := d.(string)
			if !ok {
				t.Errorf("invalid key type: key: %v, got: %T, expected: string", v.Key, d)
				t.FailNow()
			}
			exist = s == v.String
		case zapcore.Float64Type:
			f, ok := d.(float64)
			if !ok {
				t.Errorf("invalid key type: key: %v, got: %T, expected: float64", v.Key, d)
				t.FailNow()
			}
			exist = f == math.Float64frombits(uint64(v.Integer))
		}
		if !exist {
			t.Logf("field is not found: expect: %v", v)
			t.FailNow()
		}
	}

	// check unexpected fields
	for _, v := range unexpected {
		if _, ok := logData[v.Key]; ok {
			t.Errorf("got unexpect fields: key=%v", v.Key)
			t.FailNow()
		}
	}
}

type testCaseCopy struct {
	logger        bool
	sugaredLogger bool
	zapLogger     bool
	core          bool
	config        bool
	level         bool
	fields        bool
}

func checkCopy(t *testing.T, tc, name string, ptr1, ptr2 interface{}, gt bool) {
	var isCopied bool
	if ptr1 == nil || ptr2 == nil {
		isCopied = ptr1 != ptr2
	} else {
		v1 := reflect.ValueOf(ptr1)
		v2 := reflect.ValueOf(ptr2)
		switch v1.Kind() {
		// yore: use reflect.Ptr to be compatible with former versions.
		case reflect.Ptr, reflect.Chan, reflect.Map, reflect.UnsafePointer,
			reflect.Func, reflect.Slice:
			// yore: know that empty slice will not be copied even created by another make.
			isCopied = v1.Pointer() != v2.Pointer()
		default:
			isCopied = true // no ptr will always be copied
		}
	}
	if isCopied != gt {
		t.Errorf("%s: check copy for '%s' failed, old: %p, new: %p, is-copied: %v, should-copy: %v",
			tc, name, ptr1, ptr2, isCopied, gt)
		t.FailNow()
	}
}
func checkCopyLogger(t *testing.T, name string, l1, l2 *Logger, gt *testCaseCopy) {
	checkCopy(t, name, "logger", l1, l2, gt.logger)
	checkCopy(t, name, "zapSugaredLogger", l1.SugaredLogger, l2.SugaredLogger, gt.sugaredLogger)
	checkCopy(t, name, "zapLogger", l1.SugaredLogger.Desugar(), l2.SugaredLogger.Desugar(), gt.zapLogger)
	checkCopy(t, name, "zapCore", l1.SugaredLogger.Desugar().Core(), l2.SugaredLogger.Desugar().Core(), gt.core)
	checkCopy(t, name, "config", l1.cfg, l2.cfg, gt.config)
	checkCopy(t, name, "level", l1.level, l2.level, gt.config)
	if len(l1.fields) != 0 || len(l2.fields) > 0 {
		checkCopy(t, name, "fields", l1.fields, l2.fields, gt.fields)
	}
}

type testCaseLoggerNamed struct {
	name         string
	loggerNames  []string
	loggerNames2 []string
	copy         []*testCaseCopy
}

var loggerNamedTestCases = []*testCaseLoggerNamed{
	{
		name:        "logger_named_twice_check_copy_and_name",
		loggerNames: []string{"a", "b"},
		copy: []*testCaseCopy{
			{logger: true, sugaredLogger: true, zapLogger: true, core: false, config: false, level: false, fields: true},
			{logger: true, sugaredLogger: true, zapLogger: true, core: false, config: false, level: false, fields: true},
		},
	},
	{
		name:         "logger_named_check_different_name",
		loggerNames:  []string{"a", "b", "c", "d"}, // => logger: a.b.c.d
		loggerNames2: []string{"", "e", "f", ""},   // => logger: a.e.f
	},
}

func TestLogger_Named(t *testing.T) {
	checkNamedCopy := func(name string, l1, l2 *Logger, gt *testCaseCopy) {
		checkCopy(t, name, "logger", l1, l2, gt.logger)
		checkCopy(t, name, "zapSugaredLogger", l1.SugaredLogger, l2.SugaredLogger, gt.sugaredLogger)
		checkCopy(t, name, "zapLogger", l1.SugaredLogger.Desugar(), l2.SugaredLogger.Desugar(), gt.zapLogger)
		checkCopy(t, name, "zapCore", l1.SugaredLogger.Desugar().Core(), l2.SugaredLogger.Desugar().Core(), gt.core)
		checkCopy(t, name, "config", l1.cfg, l2.cfg, gt.config)
		checkCopy(t, name, "level", l1.level, l2.level, gt.config)
		// yore: ignore check fields because all the fields are empty slice, and they will have the same address.
		// checkCopy(t, c.name, "fields", l1.fields, l2.fields, gt.fields)
	}

	b := bytes.NewBufferString("")
	baseLogger1 := NewLogger(WithNormalLogOutput(b), WithErrorLogOutput(b), WithConsoleLogFormat(LogFormatJSON))
	baseLogger2 := baseLogger1
	baseName1 := ""
	baseName2 := ""
	for _, c := range loggerNamedTestCases {
		if c.copy != nil && len(c.loggerNames) != len(c.copy) {
			t.Errorf("invalid data length, loggerName: %v, copyGroundTruth: %v", len(c.loggerNames), len(c.copy))
			t.FailNow()
		}
		diff := false
		for i, name := range c.loggerNames {
			newLogger1 := baseLogger1.Named(name)
			// check copy
			if c.copy != nil {
				checkNamedCopy(c.name, baseLogger1, newLogger1, c.copy[i])
			}

			// first logger
			newName1 := name
			if baseName1 != "" {
				newName1 = strings.Join([]string{baseName1, newName1}, ".")
			}
			checkName(t, "1", newLogger1, b, newName1)

			// second logger
			var newLogger2 *Logger
			var newName2 string
			if len(c.loggerNames2) > 0 {
				name2 := c.loggerNames2[i]
				if name2 != "" {
					newLogger2 = baseLogger2.Named(name2)
					newName2 = strings.Join([]string{baseName2, name2}, ".")
					checkName(t, "2", newLogger2, b, newName2)
					diff = true
				} else if !diff {
					// just use logger1
					newLogger2 = newLogger1
					newName2 = newName1
				} // else {} // do nothing
			}

			baseLogger1 = newLogger1
			baseName1 = newName1
			baseLogger2 = newLogger2
			baseName2 = newName2
		}
	}
}

type testCaseLoggerWith struct {
	name          string
	loggerFields  []zap.Field
	loggerFields2 []zap.Field
	copy          []*testCaseCopy
}

var loggerWithTestCases = []*testCaseLoggerWith{
	{
		name: "logger_with_fields_check_copy_and_fields",
		loggerFields: []zap.Field{
			zap.String("a", "a"),
			zap.Int("b", 2),
			zap.Bool("c", true),
			zap.Float64("d", 100.0),
		},
		loggerFields2: nil,
		copy: []*testCaseCopy{
			{logger: true, sugaredLogger: true, zapLogger: true, core: true, config: false, level: false, fields: true},
			{logger: true, sugaredLogger: true, zapLogger: true, core: true, config: false, level: false, fields: true},
			{logger: true, sugaredLogger: true, zapLogger: true, core: true, config: false, level: false, fields: true},
			{logger: true, sugaredLogger: true, zapLogger: true, core: true, config: false, level: false, fields: true},
		},
	},
	{
		name: "logger_with_fields_check_different_fields",
		loggerFields: []zap.Field{
			zap.String("a", "a"),
			zap.Int("b", 2),
			zap.Bool("c", true),
			zap.Float64("d", 100.0),
		},
		loggerFields2: []zap.Field{
			{},
			{},
			zap.Bool("e", false),
			zap.Float64("f", 50.0),
		},
	},
}

func TestLogger_With(t *testing.T) {
	b := bytes.NewBufferString("")
	base := NewLogger(WithNormalLogOutput(b), WithErrorLogOutput(b), WithConsoleLogFormat(LogFormatJSON))
	for _, c := range loggerWithTestCases {
		if c.copy != nil && len(c.copy) != len(c.loggerFields) {
			t.Errorf("invalid data length, fields: %v, copyGroundTruth: %v", len(c.loggerFields), len(c.copy))
			t.FailNow()
		}

		ol1 := base
		ol2 := base
		l2Fields := make([]zapcore.Field, 0)
		l2Diff := make([]zapcore.Field, 0)
		for i, field := range c.loggerFields {
			nl1 := ol1.With(field)
			if c.copy != nil {
				checkCopyLogger(t, c.name, ol1, nl1, c.copy[i])
			}
			checkFields(t, "", nl1, b, c.loggerFields[:i+1], nil)

			var nl2 *Logger
			if len(c.loggerFields2) > 0 {
				f2 := c.loggerFields2[i]
				if f2.Type != zapcore.UnknownType {
					nl2 = ol2.With(f2)
					checkFields(t, "", nl2, b, append(l2Fields, f2), l2Diff)
					if len(l2Fields) == 0 {
						l2Fields = append(l2Fields, c.loggerFields[:i]...)
					}
				} else if len(l2Fields) == 0 {
					nl2 = nl1
				}
				if len(l2Fields) > 0 {
					l2Diff = append(l2Diff, c.loggerFields[i])
				}
			}

			ol1 = nl1
			ol2 = nl2
		}
	}
}

func TestLogger_With_SameKeyFields(t *testing.T) {
	b := bytes.NewBufferString("")
	l := NewLogger(WithNormalLogOutput(b), WithErrorLogOutput(b), WithConsoleLogFormat(LogFormatJSON))

	l = l.With("first", "1")
	l = l.With("second", 2)
	l = l.With("first", 3)
	l = l.With("second", "4")
	l = l.Fork()
	text := getLoggerOutput(l, b, zapcore.InfoLevel, "same key")
	if len(text) == 0 {
		t.Errorf("empty output")
		t.FailNow()
	}
	//t.Logf("output: %s", text)

	str := string(text)
	f1 := strings.Index(str, `"first":"1"`)
	s1 := strings.Index(str, `"second":2`)
	f2 := strings.Index(str, `"first":3`)
	s2 := strings.Index(str, `"second":"4"`)
	if f1 < 0 || s1 < 0 {
		t.Errorf("first two with lost")
		t.FailNow()
	}
	if f2 < 0 || s2 < 0 {
		t.Errorf("last two with lost")
		t.FailNow()
	}
	if f1 > s1 || f1 > f2 || f1 > s2 {
		t.Errorf("invalid index of first with of `first`")
		t.FailNow()
	}
	if s1 > f2 || s1 > s2 {
		t.Errorf("invalid index of first with of `second`")
		t.FailNow()
	}
	if f2 > s2 {
		t.Errorf("invalid index of second with of `first`")
		t.FailNow()
	}
}

func TestLogger_Fork(t *testing.T) {
	b := bytes.NewBufferString("")
	base := NewLogger(
		WithNormalLogOutput(b),
		WithErrorLogOutput(b),
		WithConsoleLogFormat(LogFormatJSON),
		// WithDevelopmentMode(), // yore: try DPanic
	)
	baseField := zap.String("base", "base")
	base = base.With(baseField)
	checkFields(t, "base", base, b, []zap.Field{baseField}, nil)

	fork := base.Fork()

	// set level
	base.SetLevel(zapcore.InfoLevel)
	fork.SetLevel(zapcore.ErrorLevel)
	checkLevel(t, "base", base, b, zapcore.InfoLevel)
	checkLevel(t, "fork", fork, b, zapcore.ErrorLevel)

	// set level after named
	b2 := base.Named("b2")
	f2 := fork.Named("f2")
	b2.SetLevel(zapcore.ErrorLevel)
	f2.SetLevel(zapcore.InfoLevel)
	checkLevel(t, "base", base, b, zapcore.ErrorLevel)
	checkLevel(t, "b2", b2, b, zapcore.ErrorLevel)
	checkName(t, "b2", b2, b, "b2")
	checkLevel(t, "fork", fork, b, zapcore.InfoLevel)
	checkLevel(t, "f2", f2, b, zapcore.InfoLevel)
	checkName(t, "f2", f2, b, "f2")

	/// set level after with fields
	b3Field := zap.String("b3", "b3")
	f3Field := zap.String("f3", "f3")
	b3 := b2.With(b3Field)
	f3 := f2.With(f3Field)
	b3.SetLevel(zapcore.InfoLevel)
	f3.SetLevel(zapcore.ErrorLevel)
	checkLevel(t, "base", base, b, zapcore.InfoLevel)
	checkLevel(t, "b2", b2, b, zapcore.InfoLevel)
	checkLevel(t, "b3", b3, b, zapcore.InfoLevel)
	checkName(t, "b3", b3, b, "b2")
	checkFields(t, "b3", b3, b, []zap.Field{b3Field, baseField}, []zap.Field{f3Field})
	checkFields(t, "b2", b2, b, []zap.Field{baseField}, []zap.Field{b3Field, f3Field})
	checkLevel(t, "fork", fork, b, zapcore.ErrorLevel)
	checkLevel(t, "f2", f2, b, zapcore.ErrorLevel)
	checkLevel(t, "f3", f3, b, zapcore.ErrorLevel)
	checkName(t, "f3", f3, b, "f2")
	checkFields(t, "f3", f3, b, []zap.Field{f3Field, baseField}, []zap.Field{b3Field})
	checkFields(t, "f2", f2, b, []zap.Field{baseField}, []zap.Field{b3Field, f3Field})

	// set parent logger level
	base.SetLevel(zapcore.ErrorLevel)
	fork.SetLevel(zapcore.InfoLevel)
	checkLevel(t, "base", base, b, zapcore.ErrorLevel)
	checkLevel(t, "b2", base, b, zapcore.ErrorLevel)
	checkLevel(t, "b3", base, b, zapcore.ErrorLevel)
	checkLevel(t, "fork", fork, b, zapcore.InfoLevel)
	checkLevel(t, "f2", fork, b, zapcore.InfoLevel)
	checkLevel(t, "f3", fork, b, zapcore.InfoLevel)
}

func TestLogger_GetLevel(t *testing.T) {
	l := NewLogger()
	level := l.GetLevel()
	if level != zapcore.InfoLevel {
		t.Errorf("get logger level failed: expected: %s, got: %s", zapcore.InfoLevel, level)
		t.FailNow()
	}
}

func TestLogger_SetLevel(t *testing.T) {
	l := NewLogger()
	l.SetLevel(zapcore.ErrorLevel)
	cur := l.GetLevel()
	if cur != zapcore.ErrorLevel {
		t.Errorf("set logger level failed: expected: %s, got: %s", zapcore.ErrorLevel, cur)
		t.FailNow()
	}
	l.SetLevel(zapcore.DebugLevel)
	cur = l.GetLevel()
	if cur != zapcore.DebugLevel {
		t.Errorf("set logger level failed: expected: %s, got: %s", zapcore.DebugLevel, cur)
		t.FailNow()
	}
}

func TestLogger_SetLevelString(t *testing.T) {
	l := NewLogger()
	if err := l.SetLevelString("error"); err != nil {
		t.Errorf("parse correct level string failed: level: %s, err: %v", "error", err)
		t.FailNow()
	}
	if err := l.SetLevelString("yore"); err == nil {
		t.Errorf("parse wrong level string failed: level: %s", "yore")
	}
}

func TestNewNoopLogger(t *testing.T) {
	defer func() {
		fail := recover()
		if fail == nil {
			t.Errorf("no-op looger should cause a panic")
			t.FailNow()
		}
	}()
	l := NewNoopLogger()
	l.Panicw("no panic")
}
