module bitbucket.org/neiku/hlog

go 1.16

require (
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/BurntSushi/toml v1.1.0 // indirect
	go.uber.org/zap v1.23.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
