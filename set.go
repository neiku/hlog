package hlog

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewSimpleLogger returns a simple logger which writes to stdout/stderr only with process id in development mode.
func NewSimpleLogger() *Logger {
	return NewLogger(
		WithLevel(zapcore.DebugLevel),
		WithDevelopmentMode(),
	).With(zap.Int("pid", os.Getpid()))
}

// NewJSONLogger returns a logger which outputs in JSON format only and keeps 10 recent log archives.
func NewJSONLogger(fileName string, debug bool) *Logger {
	opts := []Option{
		WithConsoleLogFormat(LogFormatJSON),
		WithOutputFile(fileName),
		WithOutputFileLogFormat(LogFormatJSON),
		WithOutputFileMaxSizeInMB(10),
		WithOutputFileRetainNum(10),
	}
	if debug {
		opts = append(opts, WithDevelopmentMode(), WithLevel(zapcore.DebugLevel))
	}
	return NewLogger(opts...)
}

// NewPersistentLogger returns a logger which keeps all the log file in JSON format and never cleans up.
func NewPersistentLogger(fileName string, debug bool) *Logger {
	opts := []Option{
		WithOutputFile(fileName),
		WithOutputFileLogFormat(LogFormatJSON),
		WithOutputFileMaxSizeInMB(10),
		WithOutputFileRetainNum(0),
	}
	if debug {
		opts = append(opts, WithDevelopmentMode(), WithLevel(zapcore.DebugLevel))
	}
	return NewLogger(opts...)
}
