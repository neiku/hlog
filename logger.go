package hlog

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/1set/gut/yos"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	lj "gopkg.in/natefinch/lumberjack.v2"
)

var (
	getEncoderConfig = func(lvlEnc zapcore.LevelEncoder) *zapcore.EncoderConfig {
		return &zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    lvlEnc,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.StringDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	}
	// log encoders
	consoleColorEncoder = zapcore.NewConsoleEncoder(*getEncoderConfig(zapcore.CapitalColorLevelEncoder))
	consoleMonoEncoder  = zapcore.NewConsoleEncoder(*getEncoderConfig(zapcore.CapitalLevelEncoder))
	jsonEncoder         = zapcore.NewJSONEncoder(*getEncoderConfig(zapcore.LowercaseLevelEncoder))
	// std log writers
	writeStdout  = zapcore.AddSync(os.Stdout)
	writeStderr  = zapcore.AddSync(os.Stderr)
	writeDiscard = zapcore.AddSync(ioutil.Discard)
)

// Logger is a wrapper of zap.SugaredLogger with some extra features.
type Logger struct {
	*zap.SugaredLogger
	cfg    *config
	level  *zap.AtomicLevel
	fields []interface{}
}

// LogFormat is the format of the log output.
type LogFormat string

const (
	// LogFormatConsole indicates the console format, i.e. [time] [level] [caller] [message] [fields*]
	LogFormatConsole = "console"
	// LogFormatJSON indicates the JSON format.
	LogFormatJSON = "json"
)

type config struct {
	defaultLevel zapcore.Level
	development  bool

	normalLogOutput zapcore.WriteSyncer
	errorLogOutput  zapcore.WriteSyncer
	consoleFormat   LogFormat

	outputFilePath        string
	fileLogOutput         zapcore.WriteSyncer
	outputFileFormat      LogFormat
	outputFileMaxSizeInMB uint
	outputFileCompress    bool
	outputFileRetainNum   uint

	stacktraceLevel *zapcore.Level
	monochrome      bool

	zapOpts []zap.Option
}

func defaultConfig() *config {
	return &config{
		defaultLevel:          zapcore.InfoLevel,
		development:           false,
		normalLogOutput:       writeStdout,
		errorLogOutput:        writeStderr,
		consoleFormat:         LogFormatConsole,
		outputFilePath:        "",
		fileLogOutput:         nil,
		outputFileFormat:      LogFormatJSON,
		outputFileMaxSizeInMB: 100,
		outputFileCompress:    true,
		outputFileRetainNum:   5,
		stacktraceLevel:       nil,
		monochrome:            false,
		zapOpts:               nil,
	}
}

func (c *config) buildCore(lv *zap.AtomicLevel) zapcore.Core {
	// 1. build core
	// 1.1. level enabler functions
	var normalLevel zap.LevelEnablerFunc = func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel && lv.Enabled(lvl)
	}
	var errorLevel zap.LevelEnablerFunc = func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel && lv.Enabled(lvl)
	}
	// 1.2. log level encoder
	consoleEncoder := consoleColorEncoder
	if yos.IsOnWindows() || c.monochrome {
		consoleEncoder = consoleMonoEncoder
	}
	// 1.3. build console cores
	var cores []zapcore.Core
	switch c.consoleFormat {
	case LogFormatConsole:
		cores = []zapcore.Core{
			zapcore.NewCore(consoleEncoder, c.normalLogOutput, normalLevel),
			zapcore.NewCore(consoleEncoder, c.errorLogOutput, errorLevel),
		}
	case LogFormatJSON:
		fallthrough
	default:
		cores = []zapcore.Core{
			zapcore.NewCore(jsonEncoder, c.normalLogOutput, normalLevel),
			zapcore.NewCore(jsonEncoder, c.errorLogOutput, errorLevel),
		}
	}
	// 1.4. build file core only once
	if c.fileLogOutput == nil {
		if logPath := filepath.Clean(strings.TrimSpace(c.outputFilePath)); logPath != "." {
			c.fileLogOutput = zapcore.AddSync(&lj.Logger{
				Filename:   logPath,
				MaxSize:    int(c.outputFileMaxSizeInMB),
				Compress:   c.outputFileCompress,
				MaxBackups: int(c.outputFileRetainNum),
			})
		}
	}
	if c.fileLogOutput != nil {
		switch c.outputFileFormat {
		case LogFormatConsole:
			cores = append(cores, zapcore.NewCore(consoleMonoEncoder, c.fileLogOutput, lv))
		case LogFormatJSON:
			fallthrough
		default:
			cores = append(cores, zapcore.NewCore(jsonEncoder, c.fileLogOutput, lv))
		}
	}
	// 1.5. tee cores
	return zapcore.NewTee(cores...)
}

func (c *config) build(lv *zap.AtomicLevel) *Logger {
	// 1. build core
	core := c.buildCore(lv)
	// 2. build zap options
	// 2.1. caller
	opts := []zap.Option{
		zap.AddCaller(),
	}
	// 2.2. stack trace
	if c.stacktraceLevel != nil {
		opts = append(opts, zap.AddStacktrace(c.stacktraceLevel))
	}
	// 2.3. development mode
	if c.development {
		opts = append(opts, zap.Development())
	}
	// 2.4. custom options
	if len(c.zapOpts) > 0 {
		opts = append(opts, c.zapOpts...)
	}

	// 3. build logger
	return &Logger{
		SugaredLogger: zap.New(core, opts...).Sugar(),
		cfg:           c,
		level:         lv,
		fields:        make([]interface{}, 0),
	}
}

// NewLogger creates a new Logger with the given options.
func NewLogger(options ...Option) *Logger {
	// apply options to config
	cfg := defaultConfig()
	for _, opt := range options {
		opt(cfg)
	}
	// level
	lev := zap.NewAtomicLevelAt(cfg.defaultLevel)
	// build the logger
	return cfg.build(&lev)
}

// NewNoopLogger returns a no-op logger which does nothing except panicking when calling Fatal or Panic.
func NewNoopLogger() *Logger {
	return &Logger{SugaredLogger: zap.NewNop().Sugar()}
}

// GetLevel returns the current log level.
func (l *Logger) GetLevel() zapcore.Level {
	if l.cfg == nil {
		return zapcore.InvalidLevel
	}
	return l.level.Level()
}

// SetLevel sets the log level.
func (l *Logger) SetLevel(lvl zapcore.Level) {
	if l.cfg == nil {
		return
	}
	l.level.SetLevel(lvl)
}

// SetLevelString sets the log level by string.
func (l *Logger) SetLevelString(text string) error {
	if l.cfg == nil {
		return nil
	}
	lvl, err := zapcore.ParseLevel(text)
	if err != nil {
		return err
	}
	l.SetLevel(lvl)
	return nil
}

// Fork creates a new Logger with the same options and fields.
func (l *Logger) Fork() *Logger {
	if l.cfg == nil {
		return l
	}
	lev := zap.NewAtomicLevelAt(l.level.Level())
	// just replace old core to save logger name
	logger := l.Desugar().WithOptions(zap.WrapCore(func(_ zapcore.Core) zapcore.Core {
		return l.cfg.buildCore(&lev)
	}))
	// logger fields
	fields := copyFields(l.fields)
	return &Logger{
		SugaredLogger: logger.Sugar().With(fields...),
		cfg:           l.cfg,
		level:         &lev,
		fields:        fields,
	}
}

// Named adds a sub-scope to the logger's name.
func (l *Logger) Named(name string) *Logger {
	if l.cfg == nil {
		return l
	}
	return &Logger{
		SugaredLogger: l.SugaredLogger.Named(name),
		cfg:           l.cfg,
		level:         l.level,
		fields:        copyFields(l.fields),
	}
}

// With adds a variadic number of fields to the logging context.
func (l *Logger) With(fields ...interface{}) *Logger {
	if l.cfg == nil {
		return l
	}
	return &Logger{
		SugaredLogger: l.SugaredLogger.With(fields...),
		cfg:           l.cfg,
		level:         l.level,
		fields:        copyMergeFields(l.fields, fields),
	}
}

func (l *Logger) WithOptions(_ ...Option) *Logger {
	panic("With Options is not available")
}

func copyFields(fields []interface{}) []interface{} {
	ret := make([]interface{}, len(fields))
	copy(ret, fields)
	return ret
}

func copyMergeFields(f1 []interface{}, f2 []interface{}) []interface{} {
	ret := make([]interface{}, len(f1)+len(f2))
	copy(ret, f1)
	copy(ret[len(f1):], f2)
	return ret
}
